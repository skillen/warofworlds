// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Transparent Rim Glow"
{
	Properties
	{
		_RimMultiplier("Rim Multiplier", Float) = 1
		_Colour("Colour", Color) = (1,0,0,0)
		_EmissiveMultiplier("Emissive Multiplier", Float) = 0
		_beamNoise_basecolor("beamNoise_basecolor", 2D) = "white" {}
		_TexturePanning("Texture Panning", Vector) = (0.1,0.1,0,0)
		_Tiling("Tiling", Vector) = (0.5,0.5,0,0)
		_TimeScale("Time Scale", Float) = 0.1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 4.0
		#pragma surface surf Standard keepalpha noshadow exclude_path:deferred 
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
		};

		uniform float4 _Colour;
		uniform sampler2D _beamNoise_basecolor;
		uniform float _TimeScale;
		uniform float2 _TexturePanning;
		uniform float2 _Tiling;
		uniform float _RimMultiplier;
		uniform float _EmissiveMultiplier;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float mulTime24 = _Time.y * _TimeScale;
			float2 uv_TexCoord23 = i.uv_texcoord * _Tiling;
			float2 panner16 = ( mulTime24 * _TexturePanning + uv_TexCoord23);
			float4 tex2DNode17 = tex2D( _beamNoise_basecolor, panner16 );
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = i.worldNormal;
			float fresnelNdotV1 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode1 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV1, _RimMultiplier ) );
			float temp_output_29_0 = ( tex2DNode17.r * fresnelNode1 );
			float4 temp_output_7_0 = ( _Colour * temp_output_29_0 );
			o.Albedo = temp_output_7_0.rgb;
			o.Emission = ( temp_output_7_0 * _EmissiveMultiplier ).rgb;
			o.Alpha = temp_output_29_0;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17500
7;1;1906;1021;2011.252;673.1382;1;True;True
Node;AmplifyShaderEditor.Vector2Node;27;-2022.24,-605.7027;Inherit;False;Property;_Tiling;Tiling;6;0;Create;True;0;0;False;0;0.5,0.5;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;28;-1917.985,74.20037;Inherit;False;Property;_TimeScale;Time Scale;7;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;23;-1699.24,-645.7027;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;26;-1900.24,-436.7027;Inherit;True;Property;_TexturePanning;Texture Panning;5;0;Create;True;0;0;False;0;0.1,0.1;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleTimeNode;24;-1729.825,29.52697;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-1260.49,436.6548;Float;False;Property;_RimMultiplier;Rim Multiplier;0;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;16;-1598.655,-501.5484;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FresnelNode;1;-1044.854,249.8999;Inherit;True;Standard;WorldNormal;ViewDir;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;17;-1346.237,-451.4656;Inherit;True;Property;_beamNoise_basecolor;beamNoise_basecolor;4;0;Create;True;0;0;False;0;-1;17d559dfacaa84141a8292696563cdee;17d559dfacaa84141a8292696563cdee;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-762.9998,129.5356;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;6;-996.0046,-22.17524;Inherit;False;Property;_Colour;Colour;2;0;Create;True;0;0;False;0;1,0,0,0;1,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;15;-575.6125,396.0052;Inherit;False;Property;_EmissiveMultiplier;Emissive Multiplier;3;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-455.9045,-123.4751;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;-248.1593,115.1587;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-834.9961,-356.1345;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-8.100025,66.29997;Float;False;True;-1;4;ASEMaterialInspector;0;0;Standard;Transparent Rim Glow;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;Transparent;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;2;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;23;0;27;0
WireConnection;24;0;28;0
WireConnection;16;0;23;0
WireConnection;16;2;26;0
WireConnection;16;1;24;0
WireConnection;1;3;8;0
WireConnection;17;1;16;0
WireConnection;29;0;17;1
WireConnection;29;1;1;0
WireConnection;7;0;6;0
WireConnection;7;1;29;0
WireConnection;12;0;7;0
WireConnection;12;1;15;0
WireConnection;30;1;17;1
WireConnection;0;0;7;0
WireConnection;0;2;12;0
WireConnection;0;9;29;0
ASEEND*/
//CHKSM=DBC0242181D941D216AEC3565F182C21E2E3BC8A