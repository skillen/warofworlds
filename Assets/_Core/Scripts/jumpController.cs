﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class jumpController : MonoBehaviour
{

    public int playerId = 0;
    private Player player;

    public float speed;
    public float jumpForce;
    private float moveInput;

    private Rigidbody rb;
    private Vector3 m_Velocity = Vector3.zero;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;

    private bool facingRight = true;
    public SpriteRenderer sprite;


    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        player = ReInput.players.GetPlayer(playerId);
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        float horizontal = player.GetAxis("Move Horizontal");
        float vertical = player.GetAxis("Move Vertical");

        if(!facingRight && horizontal > 0)
        {
            Flip();
        } else if (facingRight && horizontal < 0)
        {
            Flip();
        }


        // Move the character by finding the target velocity

        Vector3 localforward = Camera.main.transform.forward;
        Vector3 localright = Camera.main.transform.right;

        localforward.y = 0;
        localright.y = 0;

        localforward.Normalize();
        localright.Normalize();


        Vector3 directionVector = horizontal * localright + vertical * localforward;


        //* Time.deltaTime * 10f

        Vector3 targetVelocity = new Vector3(directionVector.x * speed, rb.velocity.y, directionVector.z * speed);

       

        // And then smoothing it out and applying it to the character
        rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);


        //float absHorizontal = Mathf.Abs(horizontal);
        //float absVertical = Mathf.Abs(vertical);
        //bool jumpDown = player.GetButtonDown("Jump");

    }

    void Flip()
    {
        facingRight = !facingRight;
        sprite.flipX = !facingRight;
    }
}
