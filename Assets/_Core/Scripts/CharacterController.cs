using Monk.TLA;
using Rewired;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;





[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody))]

public class CharacterController : MonoBehaviour
{

    public GameObject deathEffect;


    public float fallMultiplier = 3.5f;
    public float lowJumpMultiplier = 3f;


    [SerializeField] private float jumpVelocity = 5f;                          // Amount of force added when the player jumps.
    [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;          // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
    [SerializeField] private Transform m_CeilingCheck;                          // A position marking where to check for ceilings
    [SerializeField] private Collider2D m_CrouchDisableCollider;                // A collider that will be disabled when crouching

    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    [SerializeField]
    private bool m_Grounded;            // Whether or not the player is grounded.
    const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
    private Rigidbody m_Rigidbody;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;

    [SerializeField] float distToGround = 0.3f;

    public bool facingRight = true;
    public SpriteRenderer sprite;

    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    public BoolEvent OnCrouchEvent;
    private bool m_wasCrouching = false;

    



    [Header("Colliders")]
    [SerializeField] private CapsuleCollider capsuleCollider;
    [SerializeField] private List<BoxCollider> hitBoxes;
    [SerializeField] private List<BoxCollider> hurtBoxes;


    [Header("Other")]
    //[SerializeField] private KeyCode useDoorKey = KeyCode.U;
    [SerializeField] private bool billboardMainCamera = false;
    [SerializeField] private bool billboardLookAway = false;

    private static CharacterController _instance = null;
    public static CharacterController Instance { get { return _instance; } }

    private Animator _animator;
    private Rigidbody _rigid;

    private List<TLA_Door> _doorsInRange = new List<TLA_Door>();

    

    // Whatever state entity is in, this is as fast as the Rigidbody can move.
    private float _velocityClamp = Mathf.Infinity;

    private void Awake()
    {

        sprite = GetComponent<SpriteRenderer>();

        //distToGround = GetComponent<Collider>().bounds.extents.y;
        m_Rigidbody = GetComponent<Rigidbody>();
       

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();

        if (OnCrouchEvent == null)
            OnCrouchEvent = new BoolEvent();




        if (_instance == null)
        {
            _instance = this;
        }

        // Get required components.
        _animator = GetComponent<Animator>();
        _rigid = GetComponent<Rigidbody>();

      

       
    }

    void Start()
    {

    }

    void Update()
    {



        if (billboardMainCamera)
        {

            Vector3 cameraToPlayer = (Camera.main.transform.position - transform.position).normalized;
            cameraToPlayer.y = 0f;  // Flatten Y.
            transform.forward = billboardLookAway ? -cameraToPlayer : cameraToPlayer;
        }

        //// check door use.
        //if (input.getkeydown(usedoorkey) && _doorsinrange.count > 0)
        //{

        //    // move player to drop point of connected door.
        //    teleportto(_doorsinrange[0].connecteddoor.droplocation);

        //    // leave and clean up current camera trigger area.
        //    _doorsinrange[0].cameratrigger.leave();

        //    // enter new camera trigger area.
        //    _doorsinrange[0].connecteddoor.cameratrigger.enter();
        //}

        //// update door prompt.
        //HeroHUD.Instance.ToggleDoorPrompt(_doorsInRange.Count > 0);
    }

    private void FixedUpdate()
    {


        //function IsGrounded(): boolean {
        //    return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1);
        //}

      





        bool wasGrounded = m_Grounded;
        m_Grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        //Collider2D[] colliders =  
        //for (int i = 0; i < colliders.Length; i++)
        //{
        //    if (colliders[i].gameObject != gameObject)
        //    {
        //        m_Grounded = true;
        //        if (!wasGrounded)
        //            OnLandEvent.Invoke();
        //    }
        //}

        m_Grounded = Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.00001f);

       
        



        if (m_Rigidbody.velocity.y < 0)
        {
            m_Rigidbody.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (m_Rigidbody.velocity.y > 0 && !_animator.GetBool("Jump"))
        {
            m_Rigidbody.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }



        // Clamp Rigidbody velocity.
        //_rigid.velocity = Vector3.ClampMagnitude(_rigid.velocity, _velocityClamp);
    }

    public void TeleportTo(Transform t)
    {

        transform.position = t.position;
    }

   

    //destroys gameobject, probably called by the animation event
    //at the end of the die animation
    public void Die()
    {
        Vector3 bloodvector= new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1.5f, this.gameObject.transform.position.z);
        Instantiate(deathEffect, bloodvector, Quaternion.identity);
        Destroy(gameObject);
    }

   

    #region Trigger Collisions

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == LayerMask.NameToLayer("Door"))
        {

            TLA_Door door = other.GetComponent<TLA_Door>();
            if (door != null && !_doorsInRange.Contains(door))
            {
                _doorsInRange.Add(door);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.layer == LayerMask.NameToLayer("Door"))
        {

            TLA_Door door = other.GetComponent<TLA_Door>();
            if (door != null && !_doorsInRange.Contains(door))
            {
                _doorsInRange.Add(door);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.layer == LayerMask.NameToLayer("Door"))
        {

            TLA_Door door = other.GetComponent<TLA_Door>();
            if (door != null)
            {
                _doorsInRange.Remove(door);
            }
        }
    }

    #endregion

    #region Player Movement


    public void Move(float moveHorizontal, float moveVertical, bool crouch, Player player)
    {
        //Debug.Log(moveHorizontal);


        //compare user input to facing, flip if they are different
        if (!facingRight && moveHorizontal > 0)
        {
            Flip();
        }
        else if (facingRight && moveHorizontal < 0)
        {
            Flip();
        }


        // If crouching, check to see if the character can stand up
        //if (!crouch)
        //{
        //    // If the character has a ceiling preventing them from standing up, keep them crouching
        //    if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
        //    {
        //        crouch = true;
        //    }
        //}

        //Debug.Log("checking that player is grounded :" + m_Grounded);

        //only control the player if grounded or airControl is turned on
        if (m_Grounded || m_AirControl)
        {

            //Debug.Log("moving the player");

            // If crouching
            if (crouch)
            {
                if (!m_wasCrouching)
                {
                    m_wasCrouching = true;
                    OnCrouchEvent.Invoke(true);
                }

                // Reduce the speed by the crouchSpeed multiplier
                moveHorizontal *= m_CrouchSpeed;
                moveVertical *= m_CrouchSpeed;

                // Disable one of the colliders when crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = false;
            }
            else
            {
                // Enable the collider when not crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = true;

                if (m_wasCrouching)
                {
                    m_wasCrouching = false;
                    OnCrouchEvent.Invoke(false);
                }
            }

            // Move the character by finding the target velocity

            Vector3 localforward = Camera.main.transform.forward;
            Vector3 localright = Camera.main.transform.right;

            localforward.y = 0;
            localright.y = 0;

            localforward.Normalize();
            localright.Normalize();


            Vector3 directionVector = moveHorizontal * localright + moveVertical * localforward;



            //* Time.deltaTime * 10f

            Vector3 targetVelocity = new Vector3(directionVector.x , _rigid.velocity.y, directionVector.z );



            // And then smoothing it out and applying it to the character
            _rigid.velocity = Vector3.SmoothDamp(_rigid.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);


        }
        // If the player should jump...
        if (m_Grounded && _animator.GetBool("Jump"))
        {
            // Add a vertical force to the player.
            m_Grounded = false;
            m_Rigidbody.velocity += Vector3.up * jumpVelocity;


            m_Grounded = Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.00001f);

           


        }
    }

    public void EnemyMovement(float moveHorizontal, float moveVertical, bool targetOnRight)
    {
        //Debug.Log(moveHorizontal);

        if (targetOnRight && !facingRight)
        {
            Flip();
            //change rb.velocity to move towards player
        }
        else if (!targetOnRight && facingRight)
        {
            Flip();
        }

        // Move the character by finding the target velocity

        Vector3 localforward = Camera.main.transform.forward;
        Vector3 localright = Camera.main.transform.right;

        localforward.y = 0;
        localright.y = 0;

        localforward.Normalize();
        localright.Normalize();


        Vector3 directionVector = moveHorizontal * localright + moveVertical * localforward;



        //* Time.deltaTime * 10f

        Vector3 targetVelocity = new Vector3(directionVector.x, _rigid.velocity.y, directionVector.z);



        // And then smoothing it out and applying it to the character
        _rigid.velocity = Vector3.SmoothDamp(_rigid.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);




    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }




    #endregion
}