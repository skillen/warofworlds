﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBehaviour : MonoBehaviour
{

    private Animator anim;
    private CharacterController controller;

    private Rigidbody rb;

    [SerializeField]
    private Transform targetTransform;

    public float walkspeed = 5;
    public float zSpeed = 3f;

    public float runMultiplier = 2.5f;

    public float followDistance = 3.15f;
    public float distanceSlack = 0.3f;
    public float zDistanceSlack = 0.3f;
    private float chasePlayerAfterAttack = 1f;

    private float horizontalMove = 0f;
    private float verticalMove = 0f;


    private float currentAttackTime;
    private float defaultAttackTime = 2f;

    [SerializeField]
    private bool followTarget, attackTarget;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        controller = GetComponent<CharacterController>();
    }

    private void Start()
    {
        followTarget = true;
        currentAttackTime = defaultAttackTime;
    }

    private void Update()
    {
        Attack();


    }

    private void FixedUpdate()
    {

        FollowTarget();



    }

    private void FollowTarget()
    {
        if (!followTarget || targetTransform == null)
        {
            return;
        }





        //will need to turn the enemy to face towards the target
        Vector3 myLocPos = Camera.main.transform.InverseTransformPoint(targetTransform.position);
        Vector3 targetLocPos = Camera.main.transform.InverseTransformPoint(transform.position);

        bool targetToRight = (targetLocPos.x - myLocPos.x) > 0 ? false : true;
        bool targetBehind = (targetLocPos.z - myLocPos.z) > 0 ? true : false;



        float horizontal = 0;
        float vertical = 0;

        //Debug.Log(Mathf.Abs( targetTransform.position.z - transform.position.z));


        //we always align to our target on the z axis
        if (Mathf.Abs(targetLocPos.z - myLocPos.z) > zDistanceSlack)
        {
            vertical = targetBehind ? -1 : 1;
            //Debug.Log("adjusting vertical" + Mathf.Abs(targetLocPos.z - myLocPos.z));
        }


        //if player is further than attack distance, follow the player
        //ie: figure out the direction the enemy should move
        //also need to add in a bit of  a dead zone before the enemy starts moving
        if (Mathf.Abs(targetLocPos.x - myLocPos.x) > followDistance + distanceSlack)
        {

            //Debug.Log("moving towards");
            horizontal = targetToRight ? 1 : -1;

        }
        else if (Mathf.Abs(targetLocPos.x - myLocPos.x) < followDistance - distanceSlack)
        {

            //Debug.Log("moving away");
            horizontal = targetToRight ? -1 : 1;

        }
        else
        {
            horizontal = 0;

            //followTarget = false;

        }


        float absHorizontal = Mathf.Abs(horizontal);
        float absVertical = Mathf.Abs(vertical);

        //if we are moving, set anim to walk
        if ((absHorizontal >= 0.01f || absVertical >= 0.01f))
        {


            anim.SetBool("Walk", true);

            horizontalMove = horizontal * walkspeed;
            verticalMove = vertical * zSpeed;



            if (false /*if enemy is running*/)
            {

                anim.SetBool("Walk", false);
                anim.SetBool("Run", true);

                horizontalMove = horizontal * walkspeed * runMultiplier;
                verticalMove = vertical * zSpeed * runMultiplier;
            }
            else
            {
                anim.SetBool("Run", false);

            }

        }
        else
        {
            anim.SetBool("Walk", false);
            anim.SetBool("Run", false);

            horizontalMove = 0;
            verticalMove = 0;
        }

        AnimatorStateInfo currState = anim.GetCurrentAnimatorStateInfo(0);

        if (currState.IsName("Walk") || currState.IsName("MonsterWalk"))
        {
            controller.EnemyMovement(horizontalMove, verticalMove, targetToRight);
        }
           

    }

    private void changeState()
    {

    }

    private void Attack()
    {
        if (!attackTarget || targetTransform == null)
        {
            return;
        }

        currentAttackTime += Time.deltaTime;

        if (currentAttackTime > defaultAttackTime)
        {
            //do an enemy attack
        }
    }

}
