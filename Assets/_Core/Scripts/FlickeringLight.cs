﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class FlickeringLight : MonoBehaviour {

    [Header("References")]

    public Light lightSource;
    public Renderer lightRenderer;

    [Header("Settings")]

    // A that defines overall intensity (light and emission) over time. Should be normalized on both axes.
    // Curve should have intensity on Y axis and time on X axis (entire curve should fit on X between 0 and 1).
    public AnimationCurve flickerCurve;

    // This is how fast the flicker curve runs/repeats.
    [Range(0f, 10f)] public float flickerSpeed = 1f;

    // These are values that the normalized values from flicker curve should be multiplied by.
    public float lightIntensityMin = 0f;
    public float lightIntensityMax = 1f;
    [Range(0f, 10f)] public float emissionMin = 0f; //TODO: Joining these into a single inspector field "emissionRange" would be nice..
    [Range(0f, 10f)] public float emissionMax = 1f;

    public Color lightColor = Color.white;

    // Used to adjust emission property.
    private Material _lightMaterial;

    // The time to evaluate flicker curve with each update loop to get overall light intensity.
    private float _curveTime;

    private void Awake () {

        // Get the renderer material instance and set color.
        _lightMaterial = lightRenderer.material;
        _lightMaterial.color = lightColor * emissionMax;    // Multiply by emission intensity.

        // Set light color.
        lightSource.color = lightColor;
    }

    private void Update () {

        // Get overall intensity from curve at current time.
        float curveValue = flickerCurve.Evaluate(_curveTime);

        // Update time. A flicker speed of 0.5 will loop every 2 seconds, for example.
        _curveTime += Time.deltaTime * flickerSpeed;

        // Update lighting.
        _lightMaterial.SetColor("_EmissionColor", lightColor * Mathf.Min(((emissionMax * curveValue) + emissionMin), emissionMax));

        lightSource.color = lightColor;
        lightSource.intensity = lightIntensityMax * curveValue; //TODO: Also account for lightIntensityMin by adding it.
        //TODO: Also update light range.

        // Reset when we get to end of curve.
        if (_curveTime >= 1f) {
            _curveTime = 0f;
        }
    }
}
