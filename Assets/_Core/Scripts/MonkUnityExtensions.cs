﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monk.Util {

    public static class MonkUnityExtensions {

        /// <summary>
        /// Extension method to check if a layer is contained within a layer mask.
        /// </summary>
        public static bool Contains(this LayerMask mask, int layer) {
            return mask == (mask | (1 << layer));
        }
    }
}