﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleHealth : MonoBehaviour
{

    public int Health;

    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();        
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            //play die animation
            //the death animation will have an event that triggers at the end of it that removes the game object
            anim.SetBool("Dead", true);


        }
    }
}
