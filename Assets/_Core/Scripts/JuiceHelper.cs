﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Cinemachine;

// INFO
// -------------------------------------------------------------------------
// Time: If you want the screen shake to ignore changes in time scale (you 
// probably do) then you'll have to update CinemachineBrain such that 
// "Ignore Time Scale" is true and "Update Method" is Late Update.
//  - "Ignore Time Scale" is ignored for some reason in the other update
//      modes (and the source files are marked as readonly and changes
//      would be overwritten by updates anyway).
// -------------------------------------------------------------------------

/// <summary>
/// Provides an API for some useful effects/behaviours to "juice up" the game.
/// </summary>
public class JuiceHelper : MonoBehaviour {

    #region Fields - Time Manipulation
    
    [Header("Time Manipulation")]
    [Range(0.1f, 3f)] [SerializeField]
    private float timeScaleAdjustmentSpeed = 1f;

    [Header("Time Manipulation - Testing")]
    [SerializeField] private bool isTestTimeManipulation = false;
    [SerializeField] private KeyCode testTimeManipulationKey = KeyCode.J;

    [Range(0.1f, 2f)]
    [SerializeField] private float testTimeScale = 1f;

    private bool _timeScaleChangeInProgress;    // True if timescale is currently lerping to target value in a coroutine.
    private float _targetTimeScale;

    #endregion

    #region Fields - Screen Shake

    [Header("Screen Shake")]
    [SerializeField] private NoiseSettings cameraShakeNoiseProfile;

    [Range(1f, 10f)] [SerializeField]
    private float cameraShakeEaseOutSpeed = 1f;
    
    [Header("Screen Shake - Testing")]
    [SerializeField] private bool isTestScreenShake = false;
    [SerializeField] private KeyCode testScreenShakeKey = KeyCode.Space;
    [SerializeField] private float testScreenShakeDuration = 0f;
    [SerializeField] private float testScreenShakeAmplitude = 0f;
    [SerializeField] private float testScreenShakeFrequency = 0f;

    private CinemachineBrain _cinemachineBrain;
    private bool _screenShakeInProgress;    // True if camera shake is currently happening in a coroutine.
    private bool _screenShakeCameraChanged; // True if active Cinemachine virtual camera was changed WHILE a screen shake is in progress.

    #endregion

    // ------------------------------------------------

    #region MonoBehaviour

    private void Awake () {

        _cinemachineBrain = FindObjectOfType<CinemachineBrain>();
        if (!_cinemachineBrain) {
            Debug.LogWarning("JuiceHelper couldn't find CinemachineBrain in scene. Things like screen shake will not work.");
        }

        _targetTimeScale = 1f;
    }

    private void OnEnable () {

        if (_cinemachineBrain != null) {
            _cinemachineBrain.m_CameraActivatedEvent.AddListener(OnCinemachineCameraActivated);
        }
    }

    private void OnDisable () {

        if (_cinemachineBrain != null) {
            _cinemachineBrain.m_CameraActivatedEvent.RemoveListener(OnCinemachineCameraActivated);
        }
    }

    private void Update () {
        
        // Screen shake testing.
        if (isTestScreenShake && Input.GetKeyDown(testScreenShakeKey)) {
            ShakeScreen(testScreenShakeDuration, testScreenShakeAmplitude, testScreenShakeFrequency);
        }

        // Time manipulation testing.
        if (isTestTimeManipulation && Input.GetKeyDown(testTimeManipulationKey)) {
            if (Mathf.Approximately(Time.timeScale, 1f)) {
                SetTimeScale(testTimeScale, timeScaleAdjustmentSpeed);
            }
            else {  // Restore time scale.
                SetTimeScale(1f, timeScaleAdjustmentSpeed);
            }
        }
    }

    #endregion

    #region Time Scale

    /// <summary>
    /// Updates time scale to given value instantly.
    /// </summary>
    public void SetTimeScale (float newTimeScale) {
        SetTimeScale(newTimeScale, -1f);
    }

    /// <summary>
    /// Updates time scale to given value over time at given speed. If given speed is 
    /// </summary>
    public void SetTimeScale (float newTimeScale, float transitionSpeed) {

        // Do nothing if time scale coroutine is already running. //TODO: stop coroutine and start a new one?
        if (_timeScaleChangeInProgress) return;

        // Should change be instant?
        if (transitionSpeed <= 0) {
            Time.timeScale = newTimeScale;
        }
        else {
            StartCoroutine(SetTimeScaleSmooth(newTimeScale, transitionSpeed));
        }
    }

    /// <summary>
    /// Updates time scale over some unscaled time duration.
    /// </summary>
    private IEnumerator SetTimeScaleSmooth (float newTimeScale, float transitionSpeed) {

        _timeScaleChangeInProgress = true;

        while (!Mathf.Approximately(Time.timeScale, newTimeScale)) {

            Time.timeScale = Mathf.MoveTowards(
                Time.timeScale, 
                newTimeScale, 
                transitionSpeed * Time.unscaledDeltaTime
            );
            yield return null;
        }
        Time.timeScale = newTimeScale;

        _timeScaleChangeInProgress = false;
    }

    #endregion

    #region Cinemachine - Shared

    private void OnCinemachineCameraActivated (ICinemachineCamera from, ICinemachineCamera to) {

        if (_screenShakeInProgress) {
            _screenShakeCameraChanged = true;
        }
    }

    #endregion

    #region Cinemachine - Screen Shake

    public void ShakeScreen (float duration, float amplitude, float frequency) {

        // Need camera references..
        if (!_cinemachineBrain) {

            Debug.LogWarning("Screen shake failed: CinemachineBrain wasn't found in scene on Awake.");
            return;
        }

        // Need a reference to the noise profile (set via inspector).
        if (cameraShakeNoiseProfile == null) {

            Debug.LogWarning("Screen shake failed: Set noise profile in inspector.");
            return;
        }

        // Get virtual camera that is currently affecting the main camera.
        StartCoroutine(DoScreenShake(duration, amplitude, frequency));
    }

    private IEnumerator DoScreenShake (float duration, float amplitude, float frequency) {

        // If brain doesn't have an active virtual camera yet (screen shake called on Awake, Start, OnSceneLoad, etc) then
        // we'll wait a few frames to see if it gets assigned or not.
        int getActiveCameraFrameTimeout = 10;
        int framesWaited = 0;
        while (_cinemachineBrain.ActiveVirtualCamera == null && framesWaited < getActiveCameraFrameTimeout) {
            framesWaited++;
            yield return new WaitForEndOfFrame();
        }

        // If it's still null then we have to cancel..
        if (_cinemachineBrain.ActiveVirtualCamera == null) {

            Debug.LogWarning("Screen shake failed: Unable to get active virtual camera from CinemachineBrain.");
            yield break;    // Exit coroutine.
        }

        // Get reference to active virtual camera from brain.
        CinemachineVirtualCamera cam = _cinemachineBrain.ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CinemachineVirtualCamera>();

        _screenShakeInProgress = true;
        bool addedNoise = false;

        // Check if virutal camera in use has a noise component. If not, add one.
        CinemachineBasicMultiChannelPerlin noise = cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        if (noise == null) {

            noise = cam.AddCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            noise.m_NoiseProfile = cameraShakeNoiseProfile;
            addedNoise = true;
        }

        // If noise component already existed, check what values it had and store a copy for restoring when done.
        float restoreAmplitude = addedNoise ? 0f : noise.m_AmplitudeGain;
        float restoreFrequency = addedNoise ? 0f : noise.m_FrequencyGain;
        NoiseSettings restoreNoiseProfile = addedNoise ? cameraShakeNoiseProfile : noise.m_NoiseProfile;
        
        // Reset noise to start with.
        noise.m_AmplitudeGain = 0f;
        noise.m_FrequencyGain = 0f;

        //Debug.Log("<b>Starting new camera shake.</b>");

        float elapsed = 0f;
        while (elapsed < duration) {

            // If camera that's shaking is replaced with another..
            if (_screenShakeCameraChanged) {
                _screenShakeCameraChanged = false;
                break;  // Exit loop.
            }

            // Ease in/out values.
            noise.m_AmplitudeGain = amplitude;
            noise.m_FrequencyGain = frequency;

            elapsed += Time.deltaTime;
            yield return null;
        }

        // Reset noise on virtual camera we were using.
        while (Mathf.Abs(noise.m_AmplitudeGain - restoreAmplitude) > 0.1f) {

            //Debug.Log("Easing out of camera shake..");

            // Ease out of noise.
            noise.m_AmplitudeGain = Mathf.MoveTowards(noise.m_AmplitudeGain, restoreAmplitude, cameraShakeEaseOutSpeed * Time.deltaTime);
            noise.m_FrequencyGain = Mathf.MoveTowards(noise.m_FrequencyGain, restoreFrequency, cameraShakeEaseOutSpeed * Time.deltaTime);
            yield return null;
        }
        noise.m_AmplitudeGain = restoreAmplitude;
        noise.m_FrequencyGain = restoreFrequency;
        noise.m_NoiseProfile = restoreNoiseProfile;

        _screenShakeInProgress = false;

        //Debug.Log("<b>Ending camera shake.</b>");
    }

    #endregion
}






