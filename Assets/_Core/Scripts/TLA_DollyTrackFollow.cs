﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Monk.TLA {

    /// <summary>
    /// Script to allow dolly camera track to follow player on an additional axis.
    /// Eg.) If dolly track is along the world X axis, we can allow the track to follow depth as well for when player moves
    /// along the world Z axis.
    /// </summary>
    public class TLA_DollyTrackFollow : MonoBehaviour {

        public Transform followTarget;

        [Header("X")]
        public bool follow_world_x = false;
        public float offset_x = 0f;

        [Header("Y")]
        public bool follow_world_y = false;
        public float offset_y = 0f;

        [Header("Z")]
        public bool follow_world_z = false;
        public float offset_z = 0f;

        private void Start () {
            
            //if (followTarget == null && HeroEntity.Instance != null) {

            //    Debug.Log("Follow target not found for DollyTrackFollow " + name + ". Setting it to player instance as default.");
            //    followTarget = HeroEntity.Instance.transform;
            //}
        }

        private void OnDrawGizmos () {

            Gizmos.color = Color.yellow;
            //TODO
        }

        private void Update () {

            if (followTarget == null)
                return;

            Vector3 pos = transform.position;

            // Follow X
            if (follow_world_x) {
                pos.x = followTarget.position.x + offset_x;
            }

            // Follow Y
            if (follow_world_y) {
                pos.y = followTarget.position.y + offset_y;
            }

            // Follow Z
            if (follow_world_z) {
                pos.z = followTarget.position.z + offset_z;
            }

            transform.position = pos;
        }
    }
}













