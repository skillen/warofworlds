﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName= "CharStatPreset", menuName = "Scene Refs/Stats")]
public class CharStatRef : ScriptableObject
{
    public int stun;
    public int health;
}
