﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Monk.TLA {
    
    public class TLA_Door : MonoBehaviour {

        [SerializeField] private GameObject doorUpright;    // Model for toggling visualization (hide upright model when it would occlude camera view).
        [SerializeField] private GameObject doorGround;
        
        [Tooltip("The door that the player is going to or coming from.")]
        [SerializeField] private TLA_Door connectedDoor;

        [Tooltip("This is where the player ends up (position, rotation) when coming out of this door.")]
        [SerializeField] private Transform dropLocation;

        [Tooltip("This is the virtual camera to be enabled when this is the door the player is coming out of.")]
        [SerializeField] private TLA_CameraTrigger myCameraTrigger;

        public TLA_Door ConnectedDoor => connectedDoor;
        public Transform DropLocation => dropLocation;
        public TLA_CameraTrigger CameraTrigger => myCameraTrigger;

        private void Update () {

            float dot = Vector3.Dot(doorUpright.transform.forward, Camera.main.transform.forward);
            doorUpright.SetActive(dot < 0f);
            doorGround.SetActive(dot >= 0f);
        }
    }
}