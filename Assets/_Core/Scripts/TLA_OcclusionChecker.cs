﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

namespace Monk.TLA {

    public class TLA_OcclusionChecker : MonoBehaviour {

        public Transform lookTarget;
        public Vector3 lookTargetOffset;
        public LayerMask layerMask;
        public bool drawGizmos;

        /// <summary>
        /// This will hold all of the renderers for objects that are currently occluding the camera's view of the target.
        /// The renderer is what is enabled/disabled after occlusion is detected so the collider remains for further tests to see if 
        /// elements in the list are able to be shown again.
        /// </summary>
        private List<Renderer> _occludingRenderers = new List<Renderer>();

        private void Awake () {

            // If no look target set, use whatever transform this script is on.
            if (lookTarget == null) {
                lookTarget = transform;
            }
        }

        private void OnDrawGizmos () {

            if (lookTarget != null && drawGizmos) {

                Gizmos.color = Color.green;
                Gizmos.DrawWireSphere(

                    lookTarget.position + 
                        (lookTarget.right * lookTargetOffset.x) + 
                        (lookTarget.up * lookTargetOffset.y) + 
                        (lookTarget.forward * lookTargetOffset.z), 
                    0.1f
                );

                Gizmos.DrawLine(Camera.main.transform.position, lookTarget.position +
                    (lookTarget.right * lookTargetOffset.x) +
                    (lookTarget.up * lookTargetOffset.y) +
                    (lookTarget.forward * lookTargetOffset.z)
                );
            }
        }

        private void Update () {
            
            Vector3 camToTarget = Camera.main.transform.position - (lookTarget.position +
                (lookTarget.right * lookTargetOffset.x) +
                (lookTarget.up * lookTargetOffset.y) +
                (lookTarget.forward * lookTargetOffset.z));

            RaycastHit[] hits = Physics.RaycastAll(
                Camera.main.transform.position, 
                -camToTarget.normalized, 
                camToTarget.magnitude, 
                layerMask, 
                QueryTriggerInteraction.Ignore
            );

            //Debug.Log("Occlusion hit count: " + hits.Length);

            List<Renderer> stillOccludingRenderers = new List<Renderer>();
            
            // Check for new occluding objects between camera and target.
            Renderer hitRenderer = null;
            for (int i = 0; i < hits.Length; i++) {

                // Try to get the renderer that was hit.
                hitRenderer = hits[i].transform.GetComponent<Renderer>();

                // If the object is closer to camera than the player is then it is occluding.
                // Otherwise it is behind player but still being hit.
                // The distance check is probably redundant if raycast length is the length of the vector from camera to target, but it's here in case somebody changes that distance for whatever reason).
                if (hitRenderer != null && hits[i].distance <= camToTarget.magnitude) {

                    // Check if a previously occluding object is no longer blocking view of the target.
                    if (_occludingRenderers.Contains(hitRenderer)) {

                        // This one is still occluding..
                        stillOccludingRenderers.Add(hitRenderer);
                    }
                    else {  // Check if a previously un-occluding object is now blocking the camera view.

                        //Debug.Log("Found new occluding object: " + hitRenderer.name);

                        // Add new occluding object entry.
                        _occludingRenderers.Add(hitRenderer);
                        stillOccludingRenderers.Add(hitRenderer);   // Important to also add to still occluding list on first detection otherwise it'll get turned right back on below (flickering).
                        hitRenderer.enabled = false;
                    }
                }
            }
            
            // Get those no longer occluding using set difference operation.
            List<Renderer> noLongerOccluding = _occludingRenderers.Except(stillOccludingRenderers).ToList();
            //Debug.Log("No longer occluding count: " + noLongerOccluding.Count);

            // For each renderer no longer occluding view, enable again and update membership of two lists.
            for (int i = 0; i < noLongerOccluding.Count; i++) {

                //Debug.Log("Enabling " + noLongerOccluding[i].name);

                noLongerOccluding[i].enabled = true;
                _occludingRenderers.Remove(noLongerOccluding[i]);
            }
        }
    }
}