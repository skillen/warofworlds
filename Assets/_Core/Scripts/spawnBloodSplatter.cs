﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBloodSplatter : MonoBehaviour
{

    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;

    public GameObject bloodSplatter;
    // Start is called before the first frame update

    private void Awake()
    {
        part = GetComponent<ParticleSystem>();
        
    }


    void Start()
    {
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    private void OnParticleCollision(GameObject other)
    {

        int i = 0;

        //Debug.Log("blood splatter");

        

        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);

        
        while (i < numCollisionEvents)
        {
            //generate a quaternion that is aligned with the normal of the collision surface
            Quaternion NormalAlign = Quaternion.FromToRotation(Vector3.forward, collisionEvents[i].normal);
            //get location of the collision intersection
            Vector3 collisionHitLoc = collisionEvents[i].intersection;
            //spawn new instnaces of blood using the location and rotation from the collision event
            GameObject bloodInstance = Instantiate(bloodSplatter, collisionHitLoc, NormalAlign);
            //this rotates the blood splatters around their z axis randomly to make them look less uniform
            bloodInstance.transform.Rotate(NormalAlign.x, NormalAlign.y, Random.Range(0, 360), Space.Self);
            i++;
        }




        

    }
}
