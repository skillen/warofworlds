﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Monk.Util;
using Cinemachine;

namespace Monk.TLA {

    /// <summary>
    /// When the player enters a new camera trigger volume it should update the virtual camera(s) used by Cinemachine.
    /// </summary>
    public class TLA_CameraTrigger : MonoBehaviour {

        [Header("Settings")]
        public bool isStartCamera = false;

        [Header("References")]
        public CinemachineVirtualCamera virtualCamera;

        [Header("Debug")]
        [Tooltip("If true, will find attempt to find renderer for trigger volume and disable it.")]
        public bool hideVolumeOnAwake = false;

        private static TLA_CameraTrigger _mostRecentCameraTrigger = null;

        private const int c_activeCameraPriority = 100;
        private const int c_inactiveCameraPriority = 1;

        private void Awake () {
            
            if (hideVolumeOnAwake) {
                Renderer r = GetComponent<Renderer>();
                r.enabled = false;
            }

            // Default to inactive.
            if (!isStartCamera) {
                virtualCamera.Priority = c_inactiveCameraPriority;
            }
        }

        private void Start () {
            
            if (isStartCamera) {
                Enter();
            }
        }

        public void Leave () {

            DisableCamera();
            _mostRecentCameraTrigger = null;
        }

        public void Enter () {

            EnableCamera();
            _mostRecentCameraTrigger = this;
        }

        private void DisableCamera () {

            if (virtualCamera != null) {
                virtualCamera.Priority = c_inactiveCameraPriority;
            }
        }
        private void EnableCamera () {

            if (virtualCamera != null) {
                virtualCamera.Priority = c_activeCameraPriority;
            }
        }

        private void OnTriggerEnter (Collider other) {

            // If it was the player..
            if (other.tag == "Player") {

                // And this trigger's camera isn't still being used..
                if (_mostRecentCameraTrigger == null || _mostRecentCameraTrigger != this) {

                    Debug.Log("New camera triggered by player: " + name);

                    // Re-enable hidden objects from last camera trigger.
                    if (_mostRecentCameraTrigger != null) {
                        _mostRecentCameraTrigger.DisableCamera();
                    }
                    
                    // Set most recent trigger to this and hide our objects.
                    _mostRecentCameraTrigger = this;
                    _mostRecentCameraTrigger.EnableCamera();
                }
            }
        }
    }
}