﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerInputController : MonoBehaviour
{

    public int playerId = 0;

    public float walkspeed = 15;
    public float zSpeed = 10f;

    public float runMultiplier = 2.5f;

    public Animator anim;
    public CharacterController controller;

    private Player player;
    private float horizontalMove = 0f;
    private float verticalMove = 0f;

    void Awake()
    {

        anim = gameObject.GetComponent<Animator>();
        player = ReInput.players.GetPlayer(playerId);
        controller = gameObject.GetComponent<CharacterController>();

    }

    void Update()
    {

        float horizontal = player.GetAxis("Move Horizontal");
        float vertical = player.GetAxis("Move Vertical");
        float absHorizontal = Mathf.Abs(horizontal);
        float absVertical = Mathf.Abs(vertical);
        bool jumpDown = player.GetButtonDown("Jump");
        bool jumpHeld = player.GetButton("Jump");
        bool jumpUp = player.GetButtonUp("Jump");






        // Needs a bit of cleanup.
        if ((absHorizontal >= 0.01f || absVertical >= 0.01f))
        {


            anim.SetBool("Walk", true);

            horizontalMove = horizontal * walkspeed;
            verticalMove = vertical * zSpeed;



            if (player.GetButton("Run"))
            {

                anim.SetBool("Walk", false);
                anim.SetBool("Run", true);

                horizontalMove = horizontal * walkspeed * runMultiplier;
                verticalMove = vertical * zSpeed * runMultiplier;
            }
            else
            {
                anim.SetBool("Run", false);

            }

        }
        else
        {
            anim.SetBool("Walk", false);
            anim.SetBool("Run", false);

            horizontalMove = 0;
            verticalMove = 0;
        }








        // Jump.
        if (jumpDown)
        {

            anim.SetBool("Jump", true);
        }
        else if (jumpHeld)
        {

            anim.SetBool("Jump", true);
        }
        else if (jumpUp)
        {

            anim.SetBool("Jump", false);
        }


        // Parry.
        if (player.GetButtonDown("Test"))
        {

            Debug.Log("test button");
            anim.SetBool("Test", true);
            anim.SetBool("StartAttack", true);
        }

        // Parry.
        if (player.GetButtonDown("Parry"))
        {

            Debug.Log("Parry");
            anim.SetBool("Light", true);
        }

        // Debug.
        if (player.GetButtonDown("Attack"))
        {
            Debug.Log("Attack");
        }
        if (player.GetButtonDown("Grab"))
        {
            Debug.Log("Grab");
        }

        if (player.GetButtonDown("Jab"))
        {

            //Debug.Log("Jab");

            //cancel animation and increment combo depth as long as it is 3 or lower
            if (anim.GetBool("IsAttacking") && anim.GetBool("Cancelable") && (anim.GetInteger("ComboDepth") < 3))
            {

                ContinueAttackCombo(0);
            }
            //if not canceling trigger animation and reset combo
            else if (!anim.GetBool("IsAttacking"))
            {
                BeginNewAttackCombo(0);
            }

            //anim.SetBool("StartAttack", true);
            //anim.SetInteger("AtkVal", 0);
        }

        if (player.GetButtonDown("Kick"))
        {
            //Debug.Log("Kick");

            //cancel animation and increment combo depth as long as it is 3 or lower
            if (anim.GetBool("IsAttacking") && anim.GetBool("Cancelable") && (anim.GetInteger("ComboDepth") < 3))
            {
                ContinueAttackCombo(1);
            }
            //if not canceling trigger animation and reset combo
            else if (!anim.GetBool("IsAttacking"))
            {
                BeginNewAttackCombo(1);
            }

            //anim.SetBool("StartAttack", true);
            //anim.SetInteger("AtkVal", 1);
        }

        if (player.GetButtonDown("Stab"))
        {

            //Debug.Log("Stab");

            //cancel animation and increment combo depth as long as it is 3 or lower
            if (anim.GetBool("IsAttacking") && anim.GetBool("Cancelable") && (anim.GetInteger("ComboDepth") < 3))
            {
                ContinueAttackCombo(2);
            }
            //if not canceling trigger animation and reset combo
            else if (!anim.GetBool("IsAttacking"))
            {
                BeginNewAttackCombo(2);
            }

            //anim.SetBool("StartAttack", true);
            //anim.SetInteger("AtkVal", 2);
        }
    }

    private void ContinueAttackCombo(int id)
    {

        //need to check that the attack data matches up with the weapon type in future
        IncrementComboDepth();
        //pass the associated button value to attack data and get the attack from the combomanager (attackcontainer)
        //AttackData currentAttack = playerAttacks.GetAttack(calculateAttack(id));
        anim.SetBool("StartAttack", true);
        anim.SetInteger("AtkVal", calculateAttack(id));
    }

    private void BeginNewAttackCombo(int id)
    {

        ResetCombo();
        IncrementComboDepth();
        //AttackData currentAttack = playerAttacks.GetAttack(calculateAttack(id));
        anim.SetBool("StartAttack", true);
        anim.SetInteger("AtkVal", calculateAttack(id));
    }

    //put physics stuff in here
    private void FixedUpdate()
    {
        float horizontal = player.GetAxis("Move Horizontal");
        float vertical = player.GetAxis("Move Vertical");

        if (!anim.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            //controller.Move(horizontalMove * Time.deltaTime, verticalMove * Time.deltaTime, false, jump, player);
            controller.Move(horizontalMove , verticalMove, false, player);
        }
            
        //jump = false;

       
    }


    private void IncrementComboDepth()
    {
        int currentComboDepth = anim.GetInteger("ComboDepth");
        currentComboDepth++;
        anim.SetInteger("ComboDepth", currentComboDepth);
    }

    private void ResetCombo()
    {
        anim.SetInteger("ComboDepth", 0);
    }

    public void setCancelable()
    {
        anim.SetBool("Cancelable", true);
    }

    public void endCancelable()
    {
        anim.SetBool("Cancelable", false);
    }

    //calculates the position in the array where the expected attack data should be
    public int calculateAttack(int ButtonValue)
    {
        int comboPosition = anim.GetInteger("ComboDepth");
        int atkPos = ButtonValue + ((comboPosition - 1) * 3);

        //Debug.Log("attack position : " + atkPos);

        return atkPos;
    }

}
