﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class BetterJump : MonoBehaviour {

    public int PlayerId = 0;                            // Rewired Player ID
    private Player _player;                             // The Rewired Player

    public float jumpVelocity = 12f;

    public float FallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    

    Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();

        _player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        //if (_player.GetButtonDown("a"))
        //{
            


        //    //rb.AddForce(transform.up * jumpVelocity, ForceMode.Impulse);
        //}


        //Debug.Log(rb.velocity.y);
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (FallMultiplier - 1) * Time.deltaTime;
            //Debug.Log("hijump");
        }
        else if (rb.velocity.y > 0.1 && (!_player.GetButton("a")))
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
            //Debug.Log("lowjump");
        }
    }

    public void jump()
    {
        rb.velocity = transform.up * jumpVelocity;


    }
}
