﻿using UnityEngine;
using Rewired;

public class Interactable : MonoBehaviour {

    ////how close player needs to be to interact
    //public float radius = 3f;

    ////visualize radius
    //private void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.yellow;
    //    Gizmos.DrawWireSphere(transform.position, radius);
    //}


    public bool interactable = false;

    public Material[] material;
   

    private Player player;

    // Use this for initialization
    void Start()
    {
        
    }

    public virtual void Interact()
    {
        //Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (interactable && player.GetButtonDown("x"))
        {
            Interact();
        }
        if (interactable)
        {
            //rend.sharedMaterial = material[1];
        }
        else
        {
            //rend.sharedMaterial = material[0];
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            interactable = true;
            //change this to get the right player profile in the future
            player = ReInput.players.GetPlayer(0);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            interactable = false;
            player = null;
        }
    }


}
